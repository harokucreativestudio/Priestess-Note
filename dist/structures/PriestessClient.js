'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

require('dotenv').config();
var path = require('path');

var globalScope = require('./PriestessCore').globalScope.globalScope;

var server = require('./PriestessCore').PriestessCore;
var logger = require('./PriestessCore').logger;
var configConstructor = require('./PriestessCore').configConstructor;

var _require = require('body-parser'),
    urlencoded = _require.urlencoded,
    json = _require.json;

var _require2 = require('path'),
    join = _require2.join;

var morgan = require('morgan');

var Config = new configConstructor();

var PriestessClient = function () {
	function PriestessClient(port) {
		var _this = this;

		_classCallCheck(this, PriestessClient);

		this.config = Config;
		this.catchAsync = catchAsync;
		this.app = server();
		this.app.set('views', join(__dirname, './../src/views'));
		this.app.set('view engine', 'hbs');
		this.app.use(server.static(join(__dirname, '../public')));
		this.app.use(urlencoded({ extended: false }));
		readEngine(this);
		this.app.use(json());
		this.app.use(morgan('combined', { stream: logger.stream }));
		this.app.use(function (err, req, res, next) {
			res.locals.message = err.message;
			res.locals.error = req.app.get('env') === 'development' ? err : {};
			logger.error((err.status || 500) + ' - ' + err.message + ' - ' + req.originalUrl + ' - ' + req.method + ' - ' + req.ip);
			res.status(err.status || 500);
			res.render('error');
		});
		this.server = this.app.listen(port, function () {
			console.log(_this.config.nickname + ' Websocket is listening on port ' + _this.server.address().port);
		});
	}

	_createClass(PriestessClient, [{
		key: 'registerRoots',
		value: function registerRoots() {
			var _this2 = this;

			// Register Roots
			this.app.get('/', function (req, res) {
				res.render('index', { title: _this2.config.nickname + ' | Priestess Note' });
			});
			require('./libs/APIHandler')(this); // API Handler for WebSocket
			require('./libs/routeHandler')(this); // Route Handler for WebSocket
			this.app.get('*', function (req, res) {
				// 404 Handler for Websocket
				res.render('error', { title: _this2.config.nickname + ' Error Catcher', errType: '404 Not Found' });
			});
		}
	}]);

	return PriestessClient;
}();

module.exports = PriestessClient;