'use strict';

var rootDir = require('app-root-path');
var winston = require('winston');
// const { format } = require('winston');
// const { timestamp, prettyPrint } = format;

var twoDigit = '2-digit';
var dateFormat = {
	day: twoDigit,
	month: twoDigit,
	year: twoDigit,
	hour: twoDigit,
	minute: twoDigit,
	second: twoDigit
};
function formatter(args) {
	var dateTimeComponents = new Date().toLocaleTimeString('en-us', dateFormat).split(',');
	var logMessage = dateTimeComponents[0] + dateTimeComponents[1] + ' - ' + args.level + ': ' + args.message;
	return logMessage;
}

var options = {
	file: {
		level: 'info',
		filename: rootDir + '/logs/app.log',
		handleExceptions: true,
		maxsize: 5242880,
		maxFiles: 5,
		colorize: false,
		prettyPrint: true,
		formatter: formatter
	},
	console: {
		level: 'debug',
		handleExceptions: true,
		colorize: true,
		prettyPrint: true
	}
};

var logger = winston.createLogger({
	transports: [new winston.transports.File(options.file), new winston.transports.Console(options.console)],
	exitOnError: false
});

logger.stream = {
	write: function write(message, encoding) {
		logger.info(message);
	}
};

exports.logger = logger;
exports.logCore = winston;