'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Fs = require('fs-extra');
var _ = require('lodash');
var extendify = require('extendify');
var Cache = require('memory-cache');
var logger = require('../LatinaCore').logger;

var Config = function () {
    function Config() {
        _classCallCheck(this, Config);

        if (_.isNull(Cache.get('config'))) {
            Cache.put('config', this.raw());
        }
    }

    _createClass(Config, [{
        key: 'raw',
        value: function raw() {
            try {
                return importfrom('./structures/core.json');
            } catch (err) {
                if (err.code === 'MODULE_NOT_FOUND') {
                    logger.error('[ERROR] Configuration file is not detected!');
                    logger.error('[ERROR] Cancelling startup process!');
                    process.exit(1);
                }
                throw err;
            }
        }
    }, {
        key: 'get',
        value: function get(key, defaultResponse) {
            var getObject = void 0;
            try {
                getObject = _.reduce(_.split(key, '.'), function (o, i) {
                    return o[i];
                }, Cache.get('config'));
            } catch (ex) {
                _.noop();
            }

            if (!_.isUndefined(getObject)) {
                return getObject;
            }

            return !_.isUndefined(defaultResponse) ? defaultResponse : undefined;
        }
    }, {
        key: 'save',
        value: function save(json, next) {
            if (!json || !_.isObject(json) || _.isNull(json) || !_.keys(json).length) {
                logger.error('[ERROR] Invalid JSON was passed to Builder.');
                throw new Error('Invalid JSON was passed to Builder.');
            }

            Fs.writeJson('./structures/core.json', json, { spaces: 2 }, function (err) {
                if (!err) Cache.put('config', json);
                return next(err);
            });
        }
    }, {
        key: 'modify',
        value: function modify(object, next) {
            if (!_.isObject(object)) return next(new Error('Function expects an object to be passed.'));

            var deepExtend = extendify({
                inPlace: false,
                arrays: 'replace'
            });
            var modifiedJson = deepExtend(Cache.get('config'), object);

            Fs.writeJson('./config/core.json', modifiedJson, { spaces: 2 }, function (err) {
                if (err) return next(err);

                Cache.put('config', modifiedJson);
                return next();
            });
        }
    }]);

    return Config;
}();

module.exports = Config;