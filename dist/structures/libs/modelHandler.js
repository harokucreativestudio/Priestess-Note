'use strict';

require('dotenv').config();
global.importfrom = require('app-root-path').require;

var _require = require('discord.js'),
    Collection = _require.Collection;

var _require2 = require('fs'),
    readdirSync = _require2.readdirSync;

var logger = importfrom('./structures/LatinaCore.js').logger;

var ModelsCollection = new Collection();
var models = readdirSync('./src/models/');
var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
	for (var _iterator = models[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
		var model = _step.value;

		var name = model.split('.')[0];
		var file = importfrom('./src/models/' + model);
		ModelsCollection.set(name.toLowerCase(), file);
		logger.info('[INFO] ' + name + 'model loaded!');
	}
} catch (err) {
	_didIteratorError = true;
	_iteratorError = err;
} finally {
	try {
		if (!_iteratorNormalCompletion && _iterator.return) {
			_iterator.return();
		}
	} finally {
		if (_didIteratorError) {
			throw _iteratorError;
		}
	}
}

module.exports = { ModelsCollection: ModelsCollection };