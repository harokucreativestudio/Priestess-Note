"use strict";

module.exports = function (mongoose, config, logger) {
	mongoose.connect(config.database.url, {
		useNewUrlParser: true
	}).then(function () {
		logger.info("[INFO] MongoDB Mongoose Latina Driver connected, succesfully synchronized with database.");
	}).catch(function (err) {
		logger.error('[ERROR] Could not connect to the database. Exiting now...', err);
		process.exit();
	});
};