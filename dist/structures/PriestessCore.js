'use strict';

require('dotenv').config();
var mongoose = require('mongoose');
function globalScope() {
    mongoose.Promise = global.Promise;
    global.importfrom = require('app-root-path').require;
}
globalScope();

var express = require('express');
var logger = require('./libs/logEngine').logger;
var configConstructor = require('./libs/configConstructor.js');

exports.PriestessCore = express;
exports.mongoose = mongoose;
exports.logger = logger;
exports.globalScope = { globalScope: globalScope };
exports.configConstructor = configConstructor;